var gulp = require("gulp"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    nested = require("postcss-nested"),
    postcssImport = require("postcss-import"),
    mixins = require("postcss-mixins");

gulp.task("css", function(){
    return gulp.src("./app/assets/styles/style.css").pipe(postcss([postcssImport, mixins, nested,autoprefixer])).pipe(gulp.dest("./app/css/"));
});
